import {
  showlenform,
  onSuccess,
  laythongtintuform,
  hienthimonan,
} from "./controller-v2.js";
// import { dinhnghiamonan } from "./lopdoituong.js";
const base_url = "https://63f442e1de3a0b242b506338.mockapi.io";

// lấy dsmon an từ api
let dsma = () => {
  axios({ url: `${base_url}/food`, method: "GET" })
    .then((res) => {
      console.log(res);
      hienthimonan(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
dsma();
//xoa mon an
let xoamonan = (id) => {
  console.log("id: ", id);
  axios({ url: `${base_url}/food/${id}`, method: "DELETE" })
    .then((res) => {
      dsma();
      onSuccess("xóa món thành công");
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.xoamonan = xoamonan;
// thêm mon an
let themmonan = () => {
  let thongtinnhapvao = laythongtintuform();
  axios({ url: `${base_url}/food`, method: "POST", data: thongtinnhapvao })
    .then((res) => {
      console.log(res);
      dsma();
      $("#exampleModal").modal("hide");
      onSuccess("thêm món thành công");
    })
    .catch((err) => {
      console.log(err);
    });
};
window.themmonan = themmonan;
window.suaMonAn = (id) => {
  console.log("id: ", id);
  axios({ url: `${base_url}/food/${id}`, method: "GET" })
    .then((res) => {
      console.log(res.data);
      showlenform(res.data);
      $("#exampleModal").modal("show");
      dsma();
    })
    .catch((err) => {
      console.log(err);
    });
};
// cập nhật
let update = () => {
  // phải .value cho các key lấy thông tin từ form
  let ttup = laythongtintuform();
  // phải .value cho các key lấy thông tin từ form

  axios({
    url: `${base_url}/food/${ttup.id}`,
    method: "PUT",
    data: ttup,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");
      console.log(res);
      onSuccess("cập nhật thành công");
      dsma();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.update = update;
