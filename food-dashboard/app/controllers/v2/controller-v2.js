// hiển thị màn hình
export let hienthimonan = (Arrmonan) => {
  let contentHTML = " ";
  Arrmonan.forEach((item) => {
    let content = `
  <tr>
    <td>${item.id}</td>
    <td>${item.name}</td>
    <td>${item.type == "loai1" || item.type == true ? "chay" : "mặn"}</td>
    <td>${item.price}</td>
    <td>${item.discount}</td>
    <td>0</td>
    <td>${item.status ? "còn" : "hết"}</td>
    <td>
    <button class="btn btn-danger"onclick="xoamonan(${item.id})">Xoá</button>
    <button class="btn btn-success" onclick="suaMonAn(${item.id})">Sửa</button>
    </td>
  </tr>
  `;
    contentHTML += content;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};
/*
{
    "name": "Customer Data Producer",
    "type": false,
    "discount": 82468,
    "img": "https://loremflickr.com/640/480/food",
    "desc": "34175-6211",
    "price": 69031,
    "status": false,
    "id": "58",
    "ma": "Aliquam alias est au",
    "ten": "Nihil iste molestiae",
    "loai": "loai2",
    "gia": "Esse tempore imped",
    "khuyenMai": "20",
    "tinhTrang": "0",
    "hinhMon": "Laborum in voluptas ",
    "moTa": "Est dolorem id haru"
}
 */
// lấy thong tin từ form
export let laythongtintuform = () => {
  let id = document.getElementById("foodID").value;
  let name = document.getElementById("tenMon").value;
  let type = document.getElementById("loai").value;
  let price = document.getElementById("giaMon").value;
  let discount = document.getElementById("khuyenMai").value;
  let status = document.getElementById("tinhTrang").value * 1;
  let img = document.getElementById("hinhMon").value;
  let desc = document.getElementById("moTa").value;
  // chưa có .value sẽ ko nhận giá trị
  return {
    id,
    name,
    type,
    price,
    discount,
    status,
    img,
    desc,
  };
};
// hiển thị thông báo toastify có link và js
export let onSuccess = (message) => {
  Toastify({
    text: message,
    duration: 3000,
    destination: "https://github.com/apvarun/toastify-js",
    newWindow: true,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "right", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: "linear-gradient(to right, #00b09b, #96c93d)",
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};
// showlenform
export let showlenform = (monan) => {
  //show thông tin lên form

  document.getElementById("foodID").value = monan.id;
  document.getElementById("tenMon").value = monan.name;
  monan.type == "loai1" || monan.type == true
    ? (document.getElementById("loai").value = "loai1")
    : (document.getElementById("loai").value = "loai2");

  // loại 1= chay ;loại 2 =mặn ; true = chay, false mặn
  document.getElementById("giaMon").value = monan.price;
  monan.discount == "10"
    ? (document.getElementById("khuyenMai").value = "10")
    : (document.getElementById("khuyenMai").value = "20");

  // khuyến mãi true = 10, false = 20
  document.getElementById("tinhTrang").value = monan.status ? "1" : "0";
  // true =1 còn ;false =0 hết
  document.getElementById("hinhMon").value = monan.img;

  document.getElementById("moTa").value = monan.desc;
};
/*
{
    "name": "Global Tactics Coordinator",
    "type": false,
    "discount": 66230,
    "img": "https://loremflickr.com/640/480/food",
    "desc": "66909",
    "price": 71821,
    "status": true,
    "id": "61"
}
 */
